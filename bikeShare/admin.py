from django.utils import timezone

from .models import Status, Station, Bike, Ride

from .models import Station

from django.contrib import admin


class StationAdmin(admin.ModelAdmin):
    """Admin class for station object"""
    readonly_fields = ('commissionDate',)


class BikeAdmin(admin.ModelAdmin):
    """Admin class for bike object"""
    list_display = ('id', 'status')
    actions = ['repair_broken_bikes']

    def get_readonly_fields(self, request, obj=None):
        if obj:
            return 'status', 'station', 'lastRepairDate', 'commissionDate'
        else:
            return 'status', 'lastRepairDate', 'commissionDate'

    def repair_broken_bikes(self, request, queryset):
        """Repair selected broken bikes"""
        for q in queryset:
            if q.status == Status.objects.get(description='Broken'):
                q.status = Status.objects.get(description='Available')
                q.lastRepairDate = timezone.now()
                q.save()


class RideAdmin(admin.ModelAdmin):
    """Admin class for ride object"""
    list_display = ('id', 'status', 'startTime', 'endTime', 'user')
    readonly_fields = ('user', 'bike', 'status', 'startTime', 'endTime', 'startStation', 'endStation')

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


# Register your models here.
admin.site.register(Bike, BikeAdmin)
admin.site.register(Station, StationAdmin)
admin.site.register(Ride, RideAdmin)
