from django.contrib.auth.models import User
from django.db import models
from django.utils import timezone
from geoposition.fields import GeopositionField


class Status(models.Model):
    """Status of a bike"""
    description = models.CharField(max_length=200)

    def __str__(self):
        return self.description

    class Meta:
        verbose_name_plural = "statuses"


class Station(models.Model):
    """Station class"""
    position = GeopositionField()
    name = models.CharField(max_length=200)
    commissionDate = models.DateTimeField('date commissioned', default=timezone.now)
    capacity = models.IntegerField(default=0)
    status = models.ForeignKey(Status, on_delete=models.PROTECT, null=True)

    def __str__(self):
        return '%s' % self.name


class Bike(models.Model):
    """Bike class"""
    status = models.ForeignKey(Status, on_delete=models.PROTECT, null=True, default=Status.objects.get(description="Available").pk)
    lastRepairDate = models.DateTimeField('date last repaired', null=True)
    commissionDate = models.DateTimeField('date commissioned', default=timezone.now)
    station = models.ForeignKey(Station, on_delete=models.SET_NULL, null=True)

    def __str__(self):
        return self.id.__str__() # + self.status.__str__()


class Ride(models.Model):
    """Ride class connects user wih bike"""
    user = models.ForeignKey(User, on_delete=models.PROTECT)
    bike = models.ForeignKey(Bike, on_delete=models.PROTECT)
    status = models.ForeignKey(Status, on_delete=models.PROTECT, null=True)
    startTime = models.DateTimeField('date time ride start')
    endTime = models.DateTimeField('date time ride end', null=True)
    startStation = models.ForeignKey(Station, on_delete=models.PROTECT, related_name='startStation')
    endStation = models.ForeignKey(Station, on_delete=models.PROTECT, related_name='endStation', null=True)
