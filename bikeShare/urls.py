from django.conf.urls import url
from bikeShare import views

app_name = 'bikeShare'
urlpatterns = [
    # home view ex
    url(r'^$', views.home_view, name='Home'),
    # list of stations
    url(r'^stations$', views.StationsListView.as_view(), name='Stations'),
    # specific station
    url(r'^stations/(?P<station_id>[0-9]+)$', views.bikes_view, name='StationIndex'),
    # list of bikes to borrow
    url(r'^stations/(?P<station_id>[0-9]+)/borrow$', views.borrow_bike, name='Borrow'),
    # specific ride
    url(r'^rides/(?P<ride_id>[0-9]+$)', views.ride_view, name="RideIndex"),
    # all rides of a user
    url(r'^rides$', views.rides_view, name="Rides"),
    # choose station to finish ride at
    url(r'^rides/(?P<ride_id>[0-9]+)/finish$', views.finish_ride_select_station, name="FinishRideSelectStation"),
    # finish ride at selected station
    url(r'^rides/(?P<ride_id>[0-9]+)/finish/(?P<station_id>[0-9]+)$', views.finish_ride_at_station,
        name="FinishRideAtStation"),
    # report bike as broken
    url(r'^ride/(?P<ride_id>[0-9]+)/broken/(?P<bike_id>[0-9]+)$', views.report_broken, name='ReportBroken')
]
