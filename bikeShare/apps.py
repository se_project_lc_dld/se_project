from django.apps import AppConfig


class BikeShareConfig(AppConfig):
    name = 'bikeShare'
