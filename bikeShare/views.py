from django.contrib import messages
from django.contrib.auth import authenticate, login, update_session_auth_hash
from django.contrib.auth.forms import UserCreationForm, PasswordChangeForm
from django.contrib.auth.models import User
from django.core.serializers import serialize
from django.db.models import F
from django.forms import model_to_dict
from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.contrib.auth.decorators import login_required
from django.utils import timezone
from django.views import generic

from bikeShare.forms import SignUpForm
from bikeShare.models import Station, Status, Bike, Ride


AVAILABLE_BIKE_STATUS = "Available"
BORROWED_BIKE_STATUS = "Borrowed"


class StationsListView(generic.ListView):
    """View shows all stations that are online"""

    template_name = 'bikeShare/stations.html'
    context_object_name = 'online_station_list'

    def get_queryset(self):
        """Show only online stations"""
        return Station.objects.filter(status=Status.objects.get(description="Online").pk)


class StationBikesListView(generic.DetailView):
    """View shows all bikes that are at a station"""
    template_name = 'bikeShare/stationDetail.html'
    model = Station

    def get_queryset(self):
        """Show bikes available at station"""
        return Station.objects.filter(status=Status.objects.get(description='Online').pk)


@login_required
def bikes_view(request, station_id):
    """View shows all available bikes from a station"""

    station = get_object_or_404(Station, pk=station_id)
    available = Status.objects.get(description=AVAILABLE_BIKE_STATUS)
    bike_set = Bike.objects.filter(station=station.pk, status=available)

    return render(request, 'bikeShare/stationDetail.html', {
        'station': station,
        'bike_set': bike_set})


@login_required
def home_view(request):
    """Returns rides of the current user that are in progress"""

    current_user = request.user
    rides = Ride.objects.filter(user=current_user.pk, status=Status.objects.get(description="In progress").pk)

    return render(request, 'bikeShare/home.html', {
        'rides': rides,
    })


def register(request):
    """View registers a user"""
    if request.method == 'POST':
        # create form
        form = SignUpForm(request.POST)
        # validate form
        if form.is_valid():
            form.save()

            # get data from form
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')

            # create user
            user = authenticate(username=username, password=raw_password)

            # login user
            login(request, user)

            # set welcome message
            messages.add_message(request, messages.INFO, "Welcome to the bike sharing app")
            return redirect('home')
    else:
        # create form
        form = SignUpForm()
    return render(request, 'bikeShare/register.html', {'form': form})


@login_required
def borrow_bike(request, station_id):
    """Method for borrowing a bike"""

    # get the current station
    station = get_object_or_404(Station, pk=station_id)
    try:
        # get the selected bike
        selected_bike = station.bike_set.get(pk=request.POST['bike'])

    # if not found
    except (KeyError, Bike.DoesNotExist):

        # render station detail again, with error message
        return render(request, 'bikeShare/stationDetail.html', {
            'station': station,
            'bike_set': station.bike_set.filter(status=Status.objects.get(description=AVAILABLE_BIKE_STATUS)),
            'error_message': "You didn't select a bike",
        })

    else:

        # change status of bike to borrowed
        borrowed_status = Status.objects.get(description=BORROWED_BIKE_STATUS)
        selected_bike.status = borrowed_status
        selected_bike.station = None
        selected_bike.save()

        # get the current user
        current_user = request.user

        in_progress = Status.objects.get(description="In progress")
        # create a new ride
        ride = Ride(
            user=current_user,
            bike=selected_bike,
            status=in_progress,
            startTime=timezone.now(),
            startStation=station,
        )
        ride.save()

        # set message to inform user
        messages.add_message(request, messages.INFO,
                             "Borrowed bike nr." + selected_bike.id.__str__() + " from " + station.name)
        # redirect to home, to prevent double selection
        return HttpResponseRedirect(reverse('home'))


@login_required
def ride_view(request, ride_id):
    """View displays the selected ride"""

    current_ride = get_object_or_404(Ride, pk=ride_id)
    in_progress = True if current_ride.status == Status.objects.get(description="In progress") else False

    return render(request, 'bikeShare/ride.html', {
        'ride': current_ride,
        'in_progress': in_progress,
    })


@login_required
def finish_ride_select_station(request, ride_id):
    """View displays stations available to place bike at"""

    current_ride = get_object_or_404(Ride, pk=ride_id)
    error = True if current_ride.status != Status.objects.get(description="In progress") else False
    station_list = Station.objects.filter(status=Status.objects.get(description="Online"))

    return render(request, 'bikeShare/finishRide.html', {
        'station_list': station_list,
        'ride': current_ride,
        'error': error,
    })


@login_required
def finish_ride_at_station(request, ride_id, station_id):
    """Method for placing a bike at a station"""

    station = get_object_or_404(Station, pk=station_id)
    ride = get_object_or_404(Ride, pk=ride_id)
    bike = Bike.objects.get(pk=ride.bike.id)

    bike.station = station
    bike.status = Status.objects.get(description=AVAILABLE_BIKE_STATUS)
    bike.save()

    ride.endTime = timezone.now()
    ride.endStation = station
    ride.status = Status.objects.get(description="Finished")
    ride.save()

    # set message to inform user
    messages.add_message(request, messages.INFO,
                         "Returned bike nr." + bike.id.__str__() + " to station " + station.name)

    return HttpResponseRedirect(reverse('home'))


@login_required
def rides_view(request):
    """Show all rides related to a user"""

    # get the current user
    current_user = request.user

    user_rides = Ride.objects.filter(user=current_user)

    return render(request, 'bikeShare/rides.html', {
        'user_rides': user_rides
    })


@login_required
def change_password(request):
    """Change password view"""

    if request.method == "POST":

        # create form
        form = PasswordChangeForm(request.user, request.POST)

        # validate form
        if form.is_valid():
            user = form.save()
            update_session_auth_hash(request, user)

            # set message to inform user
            messages.add_message(request, messages.INFO, "Successfully changed password")
            return redirect('home')
        else:

            # set error message
            messages.error(request, "Please correct the errors below")
    else:

        # create form
        form = PasswordChangeForm(request.user)
    return render(request, 'registration/password.html', {
        'form': form
    })


@login_required
def report_broken(request, ride_id, bike_id):
    """Report bike as broken"""

    # get objects
    ride = Ride.objects.get(pk=ride_id)
    bike = Bike.objects.get(pk=bike_id)

    ride.status = Status.objects.get(description="Finished")
    ride.endTime = timezone.now()
    ride.endStation = None
    ride.save()

    bike.status = Status.objects.get(description="Broken")
    bike.station = ride.startStation
    bike.save()

    # set info message for user
    messages.add_message(request, messages.INFO, "Bike nr." + bike.id.__str__() + " reported as broken")

    return HttpResponseRedirect(reverse('home'))
